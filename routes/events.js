const express = require('express')
const router = express.Router()
const Event = require('../model/Event')

const getEvents = async (req, res, next) => {
  try {
    console.log(req.query)
    const startDate = req.query.startDate
    const endDate = req.query.endDate
    const events = await Event.find({
      $or: [{ startDate: { $gte: startDate, $lt: endDate } },
      { endDate: { $gte: startDate, $lt: endDate } }]
    }).exec()
    res.json(events)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

router.get('/', getEvents)
module.exports = router